apiVersion: apps/v1
kind: Deployment
metadata:
  name: ecryptfs-test
  namespace: openshift-kmm
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: ecryptfs-test
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      annotations:
        brz.gv.at/ecryptfs: "yes"
      creationTimestamp: null
      labels:
        app: ecryptfs-test
    spec:
      containers:
      - args:
        - bash
        - -c
        - |
          echo ===== eCryptFS Autotest
          set -e
          date --iso-8601=minutes

          exec 3> /tmp/log
          BASH_XTRACEFD=3
          trap 'e=$? ; BASH_XTRACEFD= ; echo ===== LS: ; ls -la $PD $ED ; echo ===== LOG: ; cat /tmp/log ; echo ; echo ; echo ===== ERROR $e ;  exit 1' ERR
          set -x

          PD=/pv/nfs
          ED=/ecryptfs/nfs
          EFN="$ED/NOW"
          val="$$.$RANDOM.$RANDOM.$(date +%s).$RANDOM"

          echo ===== Check mounted
          test -d $PD
          test -d $ED

          echo ===== Create empty
          > $EFN
          test ! -s $EFN
          # Filename depends on secret, do not require a specific one
          PFN=$(ls -1dt $PD/ECRYPTFS_FNEK_ENCRYPTED.* | head -1)
          ls -la $EFN $PFN || true
          test -f $PFN
          test -s $PFN # 8KB crypt headers

          echo ===== Make non-existent
          rm -f $EFN
          ls -la $EFN $PFN || true
          sleep 1
          test ! -f $PFN

          echo ===== Create with content
          echo "$val" > $EFN
          test -s $EFN
          ls -la $EFN $PFN || true
          test -f $PFN
          test -s $PFN

          echo ===== Check content
          echo "$val  =  $(cat $EFN) ??"
          test "$val" = "$(cat $EFN)"

          echo ===== If we get here, it seems to work
          echo ===== Check again tomorrow
          date --iso-8601=minutes -d tomorrow
          trap : TERM INT; sleep 86400 & wait ; exit 0
        image: XXXXXX/ubi-8
        imagePullPolicy: IfNotPresent
        name: container
        resources:
          limits:
            cpu: 100m
            memory: 50M
          requests:
            cpu: 100m
            memory: 50M
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /ecryptfs
          name: ecryptfs-base
        - mountPath: /pv/nfs
          name: pv-nfs
        - mountPath: /mnt/secret1
          name: ecryptfs-secret1
          readOnly: true
        - mountPath: /etc/brz.ecryptfs/config1/
          name: config-map1
      imagePullSecrets:
      - name: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
      nodeSelector:
        brz.gv.at/ecryptfs: node
      volumes:
      - emptyDir: {}
        name: ecryptfs-base
      - name: pv-nfs
        persistentVolumeClaim:
          claimName: some-test-pv-XXXXXXXX
      - name: ecryptfs-secret1
        secret:
          defaultMode: 420
          secretName: name-of-some-ecryptfs-secret-XXXXX
      - configMap:
          defaultMode: 420
          name: name-of-some-ecryptfs-test-configmap-XXXXX
        name: config-map1
