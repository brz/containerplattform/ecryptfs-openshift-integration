;;;; Mounting ecryptfs filesystems into a container, using OCI hooks in Kubernetes
;;;


;; Dear Compiler, give us a bit more speed but ALL safety.
(declaim (optimize (speed 1) (safety 3) (debug 0)))

;; There's also the :PROMETHEUS library, but for a single value it's not needed,
;; as we don't have any process statistics for _this_ process to transmit.
;;
;; Perhaps we might want to accumulate statistics from users and present them?
(ql:quickload '(:yason
                :cl-ppcre
                :hunchentoot
                :split-sequence))

(format t "Compiling in ~a ~a~%" (lisp-implementation-type) (lisp-implementation-version))

(require :sb-md5)
(require :sb-posix)

(defpackage :mount-in-container
  (:nicknames :m-i-c)
  (:use :cl))
(in-package :m-i-c)


(defvar *hostname* nil)
(defvar *debug* nil)

(defun debug-log (fmt &rest args)
  (when *debug*
    (let ((*print-right-margin* 1000))
      (apply #'sb-posix:syslog sb-posix:log-debug fmt args))))

(declaim (inline nice-error))
(defun nice-error (fmt &rest args)
  (let ((txt (apply #'format nil fmt args)))
    (princ txt *error-output*)
    (terpri *error-output*)
    (sb-posix:syslog sb-posix:log-err txt)
    (sb-ext:exit :code 1)))

(sb-alien:define-alien-routine  ("unshare" unshare) sb-alien:int
  (flags sb-alien:int))

;; /usr/include/linux/sched.h
(defconstant CLONE_VM             #x00000100) ;  /* set if VM shared between processes */
(defconstant CLONE_FS             #x00000200) ;  /* set if fs info shared between processes */
(defconstant CLONE_FILES          #x00000400) ;  /* set if open files shared between processes */
(defconstant CLONE_SIGHAND        #x00000800) ;  /* set if signal handlers and blocked signals shared */
(defconstant CLONE_PIDFD          #x00001000) ;  /* set if a pidfd should be placed in parent */
(defconstant CLONE_PTRACE         #x00002000) ;  /* set if we want to let tracing continue on the child too */
(defconstant CLONE_VFORK          #x00004000) ;  /* set if the parent wants the child to wake it up on mm_release */
(defconstant CLONE_PARENT         #x00008000) ;  /* set if we want to have the same parent as the cloner */
(defconstant CLONE_THREAD         #x00010000) ;  /* Same thread group? */
(defconstant CLONE_NEWNS          #x00020000) ;  /* New mount namespace group */
(defconstant CLONE_SYSVSEM        #x00040000) ;  /* share system V SEM_UNDO semantics */
(defconstant CLONE_SETTLS         #x00080000) ;  /* create a new TLS for the child */
(defconstant CLONE_PARENT_SETTID  #x00100000) ;  /* set the TID in the parent */
(defconstant CLONE_CHILD_CLEARTID #x00200000) ;  /* clear the TID in the child */
(defconstant CLONE_DETACHED       #x00400000) ;  /* Unused, ignored */
(defconstant CLONE_UNTRACED       #x00800000) ;  /* set if the tracing process can't force CLONE_PTRACE on this clone */
(defconstant CLONE_CHILD_SETTID   #x01000000) ;  /* set the TID in the child */
(defconstant CLONE_NEWCGROUP      #x02000000) ;  /* New cgroup namespace */
(defconstant CLONE_NEWUTS         #x04000000) ;  /* New utsname namespace */
(defconstant CLONE_NEWIPC         #x08000000) ;  /* New ipc namespace */
(defconstant CLONE_NEWUSER        #x10000000) ;  /* New user namespace */
(defconstant CLONE_NEWPID         #x20000000) ;  /* New pid namespace */
(defconstant CLONE_NEWNET         #x40000000) ;  /* New network namespace */
(defconstant CLONE_IO             #x80000000) ;  /* Clone io context */


(defun unshare-namespaces ()
  (flet ((f (y)
           (unless (zerop (unshare (symbol-value y)))
             (nice-error "Can't unshare ~s: ~d" y (sb-alien:get-errno)))))
    (f 'CLONE_FILES)
    (f 'CLONE_FS)
    #+(or) ;; not allowed?? not implemented in 4.18?
    (f 'CLONE_NEWUSER)))


(sb-alien:define-alien-routine  ("setns" setns) sb-alien:int
  (fd     sb-alien:int)
  (nstype sb-alien:int))

(defun switch-to-namespace (pid which)
  (with-open-file (proc-pid (format nil "/proc/~d/ns/~a" pid which))
    (setns (sb-sys:fd-stream-fd proc-pid)
           0)))

;; /usr/include/linux/keyctl.h
;; /* special process keyring shortcut IDs */
(defconstant KEY_SPEC_THREAD_KEYRING       -1) ;  /* - key ID for thread-specific keyring */
(defconstant KEY_SPEC_PROCESS_KEYRING      -2) ;  /* - key ID for process-specific keyring */
(defconstant KEY_SPEC_SESSION_KEYRING      -3) ;  /* - key ID for session-specific keyring */
(defconstant KEY_SPEC_USER_KEYRING         -4) ;  /* - key ID for UID-specific keyring */
(defconstant KEY_SPEC_USER_SESSION_KEYRING -5) ;  /* - key ID for UID-session keyring */
(defconstant KEY_SPEC_GROUP_KEYRING        -6) ;  /* - key ID for GID-specific keyring */
(defconstant KEY_SPEC_REQKEY_AUTH_KEY      -7) ;  /* - key ID for assumed request_key auth key */
(defconstant KEY_SPEC_REQUESTOR_KEYRING    -8) ;  /* - key ID for request_key() dest keyring */

(sb-alien:define-alien-routine ("syscall" syscall-3) sb-alien:long
  (number   sb-alien:long)
  (arg1     sb-alien:long)
  (arg2     sb-alien:long)
  (arg3     sb-alien:long))


(defconstant KEYCTL_LINK			8) ;	/* link a key into a keyring */
(defun link-keyrings ()
  ;; "ecryptfs-mount-private fails to initialize ecryptfs keys"
  ;; See https://bugs.launchpad.net/ubuntu/+source/ecryptfs-utils/+bug/1718658
  ;;   keyctl(KEYCTL_LINK, KEY_SPEC_USER_KEYRING, KEY_SPEC_SESSION_KEYRING) = 0
  ;;
  ;; $ printf SYS_keyctl | gcc -include sys/syscall.h -E -
  ;; 250
  (syscall-3 250 KEYCTL_LINK KEY_SPEC_USER_KEYRING KEY_SPEC_SESSION_KEYRING))


(sb-alien:define-alien-routine ("add_key" add-key) sb-alien:unsigned-int
  (type     sb-alien:c-string)
  (desc     sb-alien:c-string)
  (data     sb-alien:system-area-pointer)
  (len      sb-alien:size-t)
  (keyring  sb-alien:int))


;; /usr/include/linux/mount.h
(defconstant MS_RDONLY                  1   ) ; /* Mount read-only */
(defconstant MS_NOSUID                  2   ) ; /* Ignore suid and sgid bits */
(defconstant MS_NODEV                   4   ) ; /* Disallow access to device special files */
(defconstant MS_NOEXEC                  8   ) ; /* Disallow program execution */
(defconstant MS_SYNCHRONOUS            16   ) ; /* Writes are synced at once */
(defconstant MS_REMOUNT                32   ) ; /* Alter flags of a mounted FS */
(defconstant MS_MANDLOCK               64   ) ; /* Allow mandatory locks on an FS */
(defconstant MS_DIRSYNC               128   ) ; /* Directory modifications are synchronous */
(defconstant MS_NOSYMFOLLOW           256   ) ; /* Do not follow symlinks */
(defconstant MS_NOATIME              1024   ) ; /* Do not update access times. */
(defconstant MS_NODIRATIME           2048   ) ; /* Do not update directory access times */
(defconstant MS_BIND                 4096   )
(defconstant MS_MOVE                 8192   )
(defconstant MS_REC                 16384   )
(defconstant MS_VERBOSE             32768   ) ; /* War is peace. Verbosity is silence. MS_VERBOSE is deprecated. */
(defconstant MS_SILENT              32768   )
(defconstant MS_POSIXACL    (ash 1 16) ) ; /* VFS does not apply the umask */
(defconstant MS_UNBINDABLE  (ash 1 17) ) ; /* change to unbindable */
(defconstant MS_PRIVATE     (ash 1 18) ) ; /* change to private */
(defconstant MS_SLAVE       (ash 1 19) ) ; /* change to slave */
(defconstant MS_SHARED      (ash 1 20) ) ; /* change to shared */
(defconstant MS_RELATIME    (ash 1 21) ) ; /* Update atime relative to mtime/ctime. */
(defconstant MS_KERNMOUNT   (ash 1 22) ) ; /* this is a kern_mount call */
(defconstant MS_I_VERSION   (ash 1 23) ) ; /* Update inode I_version field */
(defconstant MS_STRICTATIME (ash 1 24) ) ; /* Always perform atime updates */
(defconstant MS_LAZYTIME    (ash 1 25) ) ; /* Update the on-disk [acm]times lazily */

(sb-alien:define-alien-routine  ("mount" mount) sb-alien:int
  (source  sb-alien:c-string)
  (target  sb-alien:c-string)
  (fs-type sb-alien:c-string)
  (flags   sb-alien:unsigned-long)
  (options sb-alien:c-string))


;; ./src/include/ecryptfs.h
(defparameter ECRYPTFS_DEFAULT_SALT_HEX "0011223344556677")
(defparameter ECRYPTFS_DEFAULT_SALT_FNEK_HEX "9988776655443322")
(defconstant ECRYPTFS_MAX_PASSWORD_LENGTH 64)

(defstruct eCryptFS-Mount
  (src           nil                            :type (or null string))
  (dst           nil                            :type (or null string))
  (fn-secret     nil                            :type (or null pathname))
  (data-secret   nil                            :type (or null pathname))
  (plaintext?    nil                            :type (member t  nil))
  (fn-enc?       t                              :type (member t  nil))
  (check-cipher? t                              :type (member t  nil))
  (chk-flagfile  "..ecryptfs.flag-file"         :type string)
  (chk-chkfile   "..ecryptfs.check-file"        :type string)
  (cipher        nil                            :type (or null string))
  (key-bytes     16                             :type (member 16 32))
  ;; gets filled upon processing
  (fn-sig        ""                             :type string)
  (data-sig      ""                             :type string))



;; This function returns a large data structure; we don't want that traced.
;; So we move it to a different package.
(defun cl-user::json-parse (input)
  (yason:parse input
               :object-key-fn (lambda (x) (intern (string-upcase x) :keyword))
               :object-as :plist))

(defun read-definition (def no-check-cipher-default ht fn)
  (destructuring-bind (&key dst src app storage
                            secret (data-secret secret) (fn-secret secret)
                            (fn-enc    t)
                            (check-cipher (not no-check-cipher-default))
                            (plaintext nil) 
                            (cipher "aes")
                            (key-bytes 16)
                            ;; TODO: error on unknown args? bad for backwards compatibility...
                            &allow-other-keys
                            ) def
    (when app
      (if dst
          (nice-error "Both \"dst\" and \"app\" given, only one allowed")
          (setf dst app)))
    (when storage
      (if src
          (nice-error "Both \"src\" and \"storage\" given, only one allowed")
          (setf src storage)))
    (unless dst
      (nice-error "No destination (application) directory in ~s" fn))
    (when (gethash dst ht)
      (nice-error "Destination ~s used twice, seen in ~s" dst fn))
    (unless src
      (nice-error "No source (storage) directory in ~s for ~s" fn dst))
    (when (and fn-enc
               (not fn-secret))
      (nice-error "Filename encryption without a secret in ~s for ~s" fn dst))
    (unless (member key-bytes '(16 32) :test #'=)
      (nice-error "Key-bytes must be 16 or 32 in ~s for ~s" fn dst))
    (when plaintext
      (warn "PLAINTEXT PASSTHROUGH ENABLED in ~s for ~s" fn dst))
    (setf (gethash dst ht)
          ;; YASON returns non-simple strings (for performance reasons);
          ;; the low-level calls (via ALIEN) need simple strings.
          (labels ((s (x)
                     (coerce x 'simple-string))
                   (dir (x)
                     ;; make exactly one slash, never mind if zero or more are there
                     (cl-ppcre:regex-replace "/*$" x "/")))
            (make-ecryptfs-mount :src (s (dir src))
                                 :dst (s (dir dst))
                                 :fn-enc? fn-enc
                                 :fn-secret   (make-pathname :directory fn-secret)
                                 :data-secret (make-pathname :directory data-secret)
                                 :plaintext?  plaintext
                                 :check-cipher? check-cipher
                                 :cipher cipher
                                 :key-bytes key-bytes)))))

(defun read-configurations (files no-check-cipher ht)
  (dolist (f files)
    (let ((defs (handler-case (cl-user::json-parse f)
                  (error (e)
                    (debug-log "parsing configuration ~s: error ~s" f e)
                    (nice-error "Can't parse configuration in ~s as JSON." f)))))
      (debug-log "conf ~s: ~s" (namestring f) defs)
      (dolist (def defs)
        (read-definition def no-check-cipher ht f)))))


(sb-alien:define-alien-routine ("ecryptfs_add_passphrase_key_to_keyring" ec-add-key) sb-alien:int
  (auth_tok_sig sb-alien:c-string)
  (secret       sb-alien:c-string)
  (salt         sb-alien:c-string))

(defvar *key-cache* (make-hash-table :test #'equal))

(defun hide-paths (files)
  (dolist (dir (remove-duplicates (mapcar #'directory-namestring files)
                                  :test #'equal))
    ;; TODO: mount an empty file only??
    ;; The secret is in some subdirectory "..xxx", though,
    ;; so just hiding the symlink wouldn't work anyway
    (unless (zerop (mount "none"
                          dir
                          "tmpfs"
                          MS_RDONLY
                          "size=0"))
      (nice-error "Cannot mount over secret directory ~s: ~d"
                  dir (sb-alien:get-errno)))))

(defun load-keys (specs)
  (labels 
      ((do-load (dir def-salt)
         ;; ECRYPTFS_PASSWORD_SIG_SIZE says 2*8, +1 for a terminating NUL...
         (let ((sig (make-array 24 
                                :element-type 'base-char
                                :initial-element #\Nul)))
           ;; We specify :latin1 to allow non-UTF8 byte sequences.
           (with-open-file (key (merge-pathnames "key" dir) :external-format :latin1)
             (let* ((len (file-length key))
                    (pass (make-string len))
                    (salt def-salt))
               ;;
               (when (> len ECRYPTFS_MAX_PASSWORD_LENGTH)
                 (nice-error "Secret in ~s is too long, eCryptFS only allows ~d characters"
                             dir ECRYPTFS_MAX_PASSWORD_LENGTH))
               (let ((got (read-sequence pass key)))
                 (unless (= len got)
                   (nice-error "Length mismatch loading secret ~s (got ~d bytes of ~d)" dir got len)))
               (when (position #\Nul pass)
                 (nice-error "The secret (in ~s) must not contain NUL" dir))
               ;;
               (let ((salt-fn (merge-pathnames "salt" dir)))
                 (when (ignore-errors (sb-posix:access salt-fn sb-posix:r-ok))
                   (with-open-file (s salt-fn)
                     (setf salt (read-line s))
                     (when (> (length salt) 16)
                       (nice-error "Salt in ~s is too long" dir))
                     (loop for ch across salt
                           unless (or (char<= #\A ch #\F)
                                      (char<= #\a ch #\f)
                                      (char<= #\0 ch #\9))
                           do (nice-error "Invalid hex-digit in salt from ~s" dir)))))
               ;;
               (let ((result (ec-add-key sig pass salt)))
                 (when (minusp result)
                   (nice-error "Secret or salt from ~s couldn't be used: error code ~d"
                               dir result)))
               ;; Clear secret
               (fill pass #\?)))
           ;; Return the key ID
           (alexandria-2:subseq* sig 0 (position #\Nul sig))))
       ;; If the same key is used in multiple mounts, load only once
       (ensure (dir def-salt)
         (alexandria:ensure-gethash dir *key-cache*
                                    (do-load dir def-salt))))
    (dolist (mount specs)
      (setf (ecryptfs-mount-data-sig mount)
            (ensure (ecryptfs-mount-data-secret mount)
                    ECRYPTFS_DEFAULT_SALT_HEX)
            ;;
            (ecryptfs-mount-fn-sig mount)
            (ensure (ecryptfs-mount-fn-secret mount)
                    ECRYPTFS_DEFAULT_SALT_FNEK_HEX)))))

(defun mount-options (mount context)
  ;; Order of arguments seems important?!
  (format nil "ecryptfs_sig=~a,ecryptfs_cipher=~a,ecryptfs_key_bytes=~d~@[,ecryptfs_fnek_sig=~a~]~@[,context=\"~a\"~]~@{~@[,~a~]~}"
          ;; context="system_u:object_r:container_file_t:s0:c4,c7",
          (ecryptfs-mount-data-sig mount)
          (ecryptfs-mount-cipher    mount)
          (ecryptfs-mount-key-bytes mount)
          (and (ecryptfs-mount-fn-enc? mount)
               (ecryptfs-mount-fn-sig mount))
          context
          (when (ecryptfs-mount-plaintext? mount)
            "ecryptfs_passthrough")
          "ecryptfs_unlink_sigs"))

(defun mount-paths (specs context)
  ;; So that FORMAT doesn't wrap
  (let ((*print-right-margin* most-positive-fixnum))
    (dolist (mount specs)
      ;; When reading JSON, these might be bigger vectors with a fill-pointer -
      ;; these are not allowed on foreign calls.
      (let ((dst (ecryptfs-mount-dst mount))
            (src (ecryptfs-mount-src mount))
            ;; TODO: R/O flag?
            (flags (logior MS_NOSUID
                           MS_NODEV
                           MS_NOEXEC
                           MS_RELATIME))
            (opt-stg (mount-options mount context)))
        (debug-log "mount ~s with ~s" dst opt-stg)
        ;; Ensure target directory exists - but only last component.
        ;; TODO make full hierarchy!!
        (unless (ignore-errors (sb-posix:access dst sb-posix:x-ok))
          (sb-posix:mkdir dst #o777))
        ;; Mount! The final moment everyone has been waiting for!
        (unless (zerop (mount src dst "ecryptfs" flags opt-stg))
          (nice-error "Cannot mount ~s at ~s with ~s: errno ~d"
                      src dst
                      opt-stg 
                      (sb-alien:get-errno)))
        (sb-posix:chmod dst #o777)))))

(defun trace-via-syslog (depth name state frame args)
  (let ((output (format nil 
                        "~a~d: ~a ~s ~s ~s"
                        (make-string (* 2 depth) :initial-element #\Space)
                        depth
                        state name
                        args frame)))
    (sb-posix:syslog sb-posix:log-debug "~a" output)
    (when (sb-unix:unix-isatty 2)
      (format *trace-output* "~a~%" output))))

(defun debug-tracing (trace?)
  (setf *debug* t)
  (when trace?
    (trace :report trace-via-syslog "M-I-C")))


(defparameter compile-version 
  #. (let ((git (sb-ext:run-program "git" '("describe" "--tags" "--dirty" "--always")
                                    :wait t
                                    :search t
                                    :output :stream)))
       (read-line (sb-ext:process-output git))))

(defparameter compile-date  #. (format nil "~{~2,'0d~}"
                                      (reverse (subseq 
                                                 (multiple-value-list 
                                                   (decode-universal-time 
                                                     (get-universal-time)))
                                                 0 6))))

(defun usage ()
  (format t "Mounts eCryptFS in Containers, version ~a, ~a ~a, built ~a.~%~%"
          compile-version 
          (lisp-implementation-type)
          (lisp-implementation-version)
          compile-date)
  (let* ((all (mapcar #'sb-vm::generation-bytes-allocated (alexandria:iota 7)))
         (s-all (apply #'+ all))
         (total (sb-ext:dynamic-space-size)))
    (format t "space: ~dK; free: ~dK; allocated: ~dK ~d~%"
            (round total 1024)
            (round (- total s-all) 1024)
            (round s-all 1024)
            all))
  (sb-ext:exit :code 0))

(defun load-libraries ()
  (let ((ecryptfs-lib (sb-ext:posix-getenv "ECRYPTFS_CONTAINER")))
    (unless ecryptfs-lib
      (usage))
    ;; Before doing anything else, load required libraries;
    ;; this needs to be done from a trusted source,
    ;; so not from the container being set up!
    ;;
    ;; The order is important.
    (dolist (lib #. (with-open-file (s "libraries.txt")
                      `(list ,@(loop for line = (read-line s nil nil)
                                    while line
                                    unless (char= #\# (aref line 0))
                                    collect (string-trim #(#\Space #\Tab #\Newline #\Linefeed #\Return)
                                                         line)))))
      (sb-alien:load-shared-object (concatenate 'string ecryptfs-lib lib)))))

(defun print-config (mount)
  (debug-log "Config for ~s:~:{ ~a=~s~}"
             (ecryptfs-mount-dst mount)
             (list .
                   #. (mapcar (lambda (slot)
                                (let ((n (sb-mop:slot-definition-name slot)))
                                  `(list ,(string-downcase (symbol-name n))
                                         (slot-value mount ',n))))
                              (sb-mop:class-slots (find-class 'ecryptfs-mount))))))

(defun report-statistics (count id)
  (multiple-value-bind (ok? t-user t-sys) (sb-unix:unix-getrusage 0) 
    (declare (ignore ok?))
    (let ((*print-right-margin* 1000))
      (sb-posix:syslog sb-posix:log-info
                       "~d eCryptFS mount~:P for ~a done (~,2f kernel, ~,2f user, ~,2f real, allocated: ~a).~%" 
                       count id
                       ;; From µsec to sec
                       (* 1d-6 t-user)
                       (* 1d-6 t-sys)
                       (/ (get-internal-real-time) 1.0d0 internal-time-units-per-second)
                       (mapcar #'sb-vm::generation-bytes-allocated (alexandria:iota 7))))))

(sb-alien:define-alien-routine ("lgetxattr" lgetxattr) sb-alien:int
  (path         sb-alien:c-string)
  (attr         sb-alien:c-string)
  (result       sb-alien:c-string)
  (buffer-len   sb-alien:size-t))

(defun read-selinux-context (path)
  (let* ((buffer (make-array 256
                             :element-type 'base-char
                             :initial-element #\Nul))
         (res (lgetxattr path "security.selinux" buffer (1- (length buffer)))))
    ;; lgetxattr("...", "security.selinux", "system_u:object_r:var_run_t:s0", 255) = 31
    (when (minusp res)
      (nice-error "Cannot get SELinux context for ~s" path))
    (alexandria-2:subseq* buffer 0 res)))


(defun selinux-context-for-ecryptfs-mount (no ecryptfs-t? s0?)
  ;; eCryptFS normally has system_u:object_r:ecryptfs_t:s0 
  ;; Container         has system_u:object_r:container_file_t:s0:c12,c38
  ;; So we need            system_u:object_r:ecryptfs_t:s0:c12,c38
  (unless no
    (let* ((container-context (read-selinux-context "/"))
           (tokens (split-sequence:split-sequence #\: container-context
                                                  :remove-empty-subseqs nil)))
      (format nil "~a:~a:~a~{:~a~}"
              (first tokens)
              (second tokens)
              (if ecryptfs-t?
                  "ecryptfs_t"
                  (third tokens))
              (if s0?
                  (list "s0")
                  (nthcdr 3 tokens))))))

(defun get-target-user ()
  ;; I'd prefer getting the target user from the CRI-O JSON,
  ;; but it's not included there.
  ;;
  ;; So we look for a purely-numeric user in /etc/passwd -
  ;; but NOT by using getent or similar stuff, as this might
  ;; be influenced by the (containers!!) /etc/resolv.conf etc. -
  ;; and that might be a security risk!
  (with-open-file (s "/etc/passwd")
    (loop for line = (read-line s nil nil)
          while line
          ;; Why is group zero being used?
          ;;   1001430000:x:1001430000:0:1001430000 user:/:/sbin/nologin
          for (name pswd id . rest) = (split-sequence:split-sequence #\: line)
          for uid = (ignore-errors (parse-integer id))
          thereis (and (string= name id)
                       uid))))


(sb-alien:define-alien-routine  ("setgroups" setgroups) sb-alien:int
  (count sb-alien:size-t)
  (vec sb-alien:system-area-pointer))

;; sys/prctl.h
(defconstant PR-CAP-AMBIENT 47)
(defconstant PR-CAP-AMBIENT-CLEAR-ALL 4)

(sb-alien:define-alien-routine  ("prctl" prctl) sb-alien:int
  (option sb-alien:int)
  (arg2 sb-alien:unsigned-long)
  (arg3 sb-alien:unsigned-long)
  (arg4 sb-alien:unsigned-long)
  (arg5 sb-alien:unsigned-long))

          
(defun drop-to-user (user)
  ;; Drop additional groups, keep only the user-ID as group
  ;; and group 0 (kubernetes-convention)
  (let ((my-groups (make-array 2 :element-type 'sb-unix:unix-gid
                               :initial-contents (list user 0))))
    (setgroups (length my-groups)
               (sb-sys:vector-sap my-groups)))
  ;; Openshift uses the same number for User and Group.
  (sb-posix:setresgid user user user)
  (sb-posix:setresuid user user user)
  ;; Drop capabilities
  (assert (zerop
            (prctl PR-CAP-AMBIENT
                   PR-CAP-AMBIENT-CLEAR-ALL
                   0 0 0))))

(defun check-flag-file (mount)
  ;; We avoid known-plaintext attacks by writing an odd number
  ;; of random bytes and their MD5.
  ;;
  ;; As the filenames are encrypted by default as well,
  ;; we need to create a flag file saying "initialized"
  ;; in the clear-text directory as well --
  ;; with the wrong secret we wouldn't see the other (older) files
  ;; and would just create new flag files in the encrypted directory!
  ;;
  ;; Just writing the ecryptfs Key ID might be a bad idea -
  ;; that potentially leaks information about the key on the 
  ;; unencrypted volume!
  (when (ecryptfs-mount-check-cipher? mount)
    (let* ((dest-fn (merge-pathnames (ecryptfs-mount-chk-chkfile mount)
                                     (ecryptfs-mount-dst mount)))
           (flag-fn (merge-pathnames (ecryptfs-mount-chk-flagfile mount)
                                     (ecryptfs-mount-src mount)))
           (size 41)
           (rnd (make-array size :element-type '(unsigned-byte 8)))
           (chk (make-array 16 :element-type '(unsigned-byte 8))))
      (flet ((read-seq (x s)
               (when (/= (length x)
                         (read-sequence x s))
                 (nice-error "File ~s cannot be read" dest-fn)))
             (checksum (x)
               (sb-md5:md5sum-sequence x)))
        (cond
          ((not (probe-file flag-fn))
           ;; doesn't exist yet...
           (setf *random-state* 
                 (sb-ext:seed-random-state t))
           (dotimes (i size)
             (setf (aref rnd i)
                   (random 256)))
           (setf chk (checksum rnd))
           (with-open-file (s dest-fn
                              :direction         :output
                              :if-does-not-exist :create
                              :if-exists         :supersede ; shouldn't happen
                              :element-type      '(unsigned-byte 8))
             (write-sequence rnd s)
             (write-sequence chk s))
           ;; In case some funny one happens to configure both files
           ;; to the same name AND deactivates file name encryption,
           ;; the flag- and check-file names collide here.
           ;; So DON'T drop the contents that we just wrote, just touch it!
           (with-open-file (s flag-fn 
                              :direction         :output
                              :if-does-not-exist :create
                              :if-exists         :append
                              :element-type      '(unsigned-byte 8))
             ;; Just don't write anything.
             )
           (debug-log "Created a new ecryptfs check file at ~s, checksum [~{~2,'0x~^ ~}]"
                      dest-fn
                      (coerce chk 'list)))
          ((not (probe-file dest-fn))
           ;; Sync or consistency error!
           (nice-error "Encryption check file ~s not found, but flag-file ~s exists?! Probably wrong secret configured." 
                       dest-fn flag-fn))
          (t
           ;; both exist, check integrity
           (with-open-file (s dest-fn
                              :direction :input
                              :element-type '(unsigned-byte 8))
             (read-seq rnd s)
             (read-seq chk s)
             (when (read-byte s nil nil)
               (nice-error "Encryption check file ~s damaged." 
                           dest-fn))
             (unless (equalp chk (checksum rnd))
               (nice-error "Directory ~s configured with wrong secret."
                           (ecryptfs-mount-dst mount))))))))))

(defun do-ecryptfs (input)
  ;; We expect that to be well-formed always... no nice error reporting needed
  (let* ((oci-data (cl-user::json-parse input))
         (mounts (make-hash-table :test #'equal)))
    (destructuring-bind (&key id pid annotations &allow-other-keys) oci-data
      (destructuring-bind (&key io.kubernetes.cri-o.mountpoint (brz.gv.at/ecryptfs "defaults") &allow-other-keys) annotations
        (when (search "trace" brz.gv.at/ecryptfs)
          (debug-tracing t))
        (when (search "debug" brz.gv.at/ecryptfs)
          (debug-tracing nil))
        (debug-log "from oci: pid ~d, mountpoint ~s, flags ~s"
                   pid io.kubernetes.cri-o.mountpoint brz.gv.at/ecryptfs)
        ;; Load before switching namespaces!
        (load-libraries)
        ;; Switch to namespace of container
        (unshare-namespaces)
        (switch-to-namespace pid "mnt")
        (sb-posix:chroot (coerce io.kubernetes.cri-o.mountpoint 'simple-string))
        (sb-posix:chdir "/")
        ;; Not Implemented?
        #+(or)
        (switch-to-namespace pid "user")
        ;; Read config, skipping any hidden files or directories.
        (read-configurations (remove-if (lambda (path)
                                          (search "/." (namestring path)))
                                        (directory #P"/etc/brz.ecryptfs/**/config-map"
                                                   :resolve-symlinks nil))
                             (search "no-check-cipher" brz.gv.at/ecryptfs)
                             mounts)
        (let ((mount-list (alexandria:hash-table-values mounts))
              (target-user (get-target-user))
              (wanted-context (selinux-context-for-ecryptfs-mount
                                ;; TODO deactivate, can only break stuff?
                                (search "selinux-no" brz.gv.at/ecryptfs)
                                (search "selinux+ecryptfs" brz.gv.at/ecryptfs)
                                (search "selinux+s0" brz.gv.at/ecryptfs))))
          ;; Print config
          (when *debug*
            ;; Because SORT may "damage" the list, we need to work on a copy.
            (dolist (mount (sort (copy-seq mount-list)
                                 #'string<
                                 :key #'ecryptfs-mount-dst))
              (print-config mount)))
          ;; Act on config
          (link-keyrings)
          (load-keys mount-list)
          (unless (search "keep-secrets" brz.gv.at/ecryptfs)
            (hide-paths (concatenate 
                          'list
                          (mapcar #'ecryptfs-mount-fn-secret   mount-list)
                          (mapcar #'ecryptfs-mount-data-secret mount-list))))
          ;; Shorted path to longest path, in case some stuff gets encrypted twice
          (mount-paths 
            (sort (copy-seq mount-list)
                  #'<
                  :key (lambda (m)
                         (length (ecryptfs-mount-dst m))))
            wanted-context)
          ;; Drop all priviledges.
          ;; /etc/passwd might not contain the expected line (eg. for build containers)
          ;; but we don't want uid 0 any more!
          (drop-to-user (or target-user 1))
          ;; Not entirely sure whether that is a good idea --
          ;; using a privileged user to access
          (mapcar #'check-flag-file 
                  mount-list)
          (report-statistics 
            (hash-table-count mounts)
            id))))))

(defun install-hook (dir binary)
  (let ((crio-id nil)
        (mem-cg-path nil))
    (with-open-file (s "/proc/self/cgroup")
      (loop for line = (read-line s nil nil)
            while line
            ;; CRUN (https://www.redhat.com/sysadmin/introduction-crun)
            ;; has an additional directory level in the cgroup
            for match = (or
                          (nth-value 1 
                                     (cl-ppcre:scan-to-strings
                                       "^\\d+:memory:(/kubepods.slice/[a-zA-Z0-9._/-]+/*crio-(\\w+)\\.scope)$"
                                       line))
                          (nth-value 1 
                                     (cl-ppcre:scan-to-strings
                                       "^\\d+:memory:(/kubepods.slice(?:/[a-zA-Z0-9._/-]+)+/*crio-(\\w+)\\.scope/container)$"
                                       line)))
            when match
            do (setf mem-cg-path (aref match 0)
                     crio-id (aref match 1))))
    (unless (and crio-id mem-cg-path)
      (nice-error "Cannot determine CRIO ID and memory cgroup name"))
    (format t "My CRIO ID: ~a~%" crio-id)
    ;; Fetch hostname (debugging purposes, mostly)
    (let* ((proc (sb-ext:run-program "nsenter"
                                     (list "--target" "1"
                                           "--all"
                                           "hostname")
                                     :wait nil
                                     :search t 
                                     :output :stream
                                     :error *error-output*))
           (hostname (read-line (sb-ext:process-output proc) nil nil)))
      (sb-ext:process-wait proc)
      (let ((exit-code (sb-ext:process-exit-code proc)))
        (unless (zerop exit-code)
          (nice-error "Exit code from \"hostname\" in host namespace: ~d~%" exit-code)))
      (when hostname
        (setf *hostname* hostname)))
    ;; Get container information to install OCI hook
    (let* ((crictl (sb-ext:run-program "nsenter"
                                       (list "--target" "1"
                                             "--all"
                                             "crictl" "inspect" crio-id)
                                       ;; must not wait, might be a lot of output 
                                       ;; that can't be pushed into the pipe in one go
                                       :wait nil
                                       :search t 
                                       :output :stream
                                       :error *error-output*))
           (json (yason:parse (sb-ext:process-output crictl)
                              :object-as :hash-table)))
      (sb-ext:process-wait crictl)
      (when (sb-ext:process-alive-p crictl)
        (nice-error "crictl didn't stop"))
      (let ((exit-code (sb-ext:process-exit-code crictl)))
        (unless (zerop exit-code)
          (nice-error "Exit code from \"crictl\" in host namespace: ~d~%" exit-code)))
      (let* ((info (gethash "info" json))
             (spec (gethash "runtimeSpec" info))
             (ann  (gethash "annotations" spec))
             (mountpoint (gethash "io.kubernetes.cri-o.MountPoint" ann))
             ;;
             (fn (format nil "~a/ecryptfs-hook-~a.json"
                         dir
                         (subseq crio-id 0 12))))
        (unless mountpoint
          (nice-error "Can't get own mountpoint from crio"))
        (with-open-file (hook-stream fn
                                     :direction :output
                                     :if-exists :supersede
                                     :if-does-not-exist :create)
          (let ((yason:*list-encoder* #'yason:encode-alist))
            (yason:with-output (hook-stream :indent 4)
              (yason:encode `(("version" . "1.0.0")
                              ("hook" . (("path" . ,(format nil "~a/~a" mountpoint binary))
                                         ("env" . #(,(format nil "ECRYPTFS_CONTAINER=~a" mountpoint)
                                                     ,(format nil "ECRYPTFS_MEMCG=~a" mem-cg-path)))))
                              ("when" . (("annotations" . (("brz.gv.at/ecryptfs" . "")))))
                              ("stages" . #("createRuntime")))
                            ))))
        fn))))


(defun find-ecryptfs-module-usage ()
  (with-open-file (str "/proc/modules")
    (loop for line = (read-line str nil nil)
          while line
          for parts = (split-sequence:split-sequence #\Space line)
          if (equal (first parts) "ecryptfs")
          do (return (parse-integer (third parts))))))


(hunchentoot:define-easy-handler (prometheus-data :uri "/metrics")
    ()
  (let ((use (find-ecryptfs-module-usage)))
    (format nil
            "# HELP ecryptfs_mounts Number of active eCryptFS mounts in use.~@
            # TYPE ecryptfs_mounts gauge~@
            ecryptfs_mounts ~d~@
            ~@
            # HELP ecryptfs_instance Value 1, so that the sum gives number of active eCryptFS userspace instances.~@
            # TYPE ecryptfs_instance gauge~@
            ecryptfs_instance 1~%"
            use)))

(defun run-daemon (port)
  (let* ((acceptor (make-instance 'hunchentoot:easy-acceptor
                                  :port port
                                  :access-log-destination (make-broadcast-stream)
                                  :message-log-destination *standard-output*))
         (use (find-ecryptfs-module-usage)))
    (unless use
      (nice-error "No ecryptfs kernel module loaded.~%Node ~a;~%~a~%"
                  *hostname*
                  (alexandria:read-file-into-string "/proc/version")))
    (format t "Starting liveness server (~a, built ~a), hostname ~a, current usage ~d.~%"
            compile-version
            compile-date
            *hostname*
            use)
    (hunchentoot:start acceptor)
    (loop
      (sleep 88188))))


(defun main (&key (input *standard-input*))
  ;; Must be done very early -- the containers' /dev/log would be the wrong one or does not even exist!
  (sb-posix:openlog (format nil "brz-ecryptfs-setup(~a)" compile-version)
                    (logior sb-posix:log-ndelay
                            sb-posix:log-nowait
                            ;; sb-posix:log-perror ;; don't log to STDERR too, else crio gets spammed with trace messages!
                            sb-posix:log-pid
                            sb-posix:log-cons)
                    sb-posix:log-daemon)
  ;; TODO: drop capabilities?
  ;; TODO: seccomp?
  ;; Remove binary name (equiv. $0).
  (let* ((self     (pop sb-ext:*posix-argv*))
         (arg1     (pop sb-ext:*posix-argv*))
         (arg2     (pop sb-ext:*posix-argv*))
         (arg2-num (ignore-errors (parse-integer arg2)))
         (arg3     (pop sb-ext:*posix-argv*))
         (arg4     (pop sb-ext:*posix-argv*)))
    (cond
      ((and arg1 (string= arg1 "--daemon") arg2-num arg3 arg4)
       ;; started as daemonset container process
       (let ((hook-fn (install-hook arg3 arg4)))
         (unwind-protect
             (run-daemon arg2-num)
           (delete-file hook-fn))
         (sb-ext:exit :code 0)))
      ((or (and arg1 arg2 arg3
                (string= arg1 "--install")))
       (let ((hook-fn (install-hook arg2 arg3)))
         (print hook-fn))
       (sb-ext:exit :code 0))
      ((or (and arg1
                (string= arg1 "--load-libs")))
       (load-libraries)
       (sb-ext:exit :code 0))
      ((member arg1 '("--libs" "--licenses")
               :test #'equalp)
       (let ((ll (make-hash-table :test #'equal)))
         (asdf:map-systems (lambda (sys)
                             (let* ((name (asdf/component:component-name sys))
                                    (lic (asdf:system-license sys)))
                               (when (and (asdf::primary-system-p sys)
                                          (not (cl-ppcre:scan "[/-]tests?$" name)))
                                 (pushnew name
                                          (gethash lic ll ()))))))
         (format t "Libraries by license:~%")
         (loop :for (lic . syss) :in (sort (alexandria:hash-table-alist ll)
                                           #'string<
                                           :key #'car)
               do (format t "  ~a: ~{~a~^, ~}~%"
                          lic
                          (sort syss #'string<)))
         (sb-ext:exit :code 0)))
      ;; Tracing requested?
      ((or (and arg1
                (string= arg1 "debug")))
       (pop sb-ext:*posix-argv*)
       (debug-tracing nil))
      ((or (and arg1
                (string= arg1 "trace")))
       (pop sb-ext:*posix-argv*)
       (debug-tracing t))
      ((sb-ext:posix-getenv "ECRYPTFS_TRACE")
       (debug-tracing t))))
  ;;
  (when (or sb-ext:*posix-argv*
            ;; STDIN must not be a TTY - we expect JSON input!
            (plusp (sb-unix:unix-isatty 0)))
    (usage))
  (handler-case
      (do-ecryptfs input)
    (error (c)
      (sb-posix:syslog sb-posix:log-err "Error: ~a" c)
      (sb-ext:exit :code 1))))

(defun shake-and-save (fn)
  ;; Now produce a binary.
  (sb-ext:save-lisp-and-die fn
                            :toplevel #'main
                            :executable t
                            :purify t
                            :save-runtime-options T
                            :compression nil))


;; We want to use the target pid mount namespace;
;; but when we reach it, we must not use the "mount" program,
;; as this might be compromised!
;;
;; So we need a binary to call the glibc functions.


(shake-and-save (or (second sb-ext:*posix-argv*)
                              "mount-in-container"))
