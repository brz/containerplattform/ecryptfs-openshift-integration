#!/bin/bash

# escape the memory restrictions of the target container
# that this late-order OCI hook already gets imposed
if [[ -n "$ECRYPTFS_MEMCG" ]]
then
	cg_dir="/sys/fs/cgroup/memory/$ECRYPTFS_MEMCG"

	if [[ -d "$cg_dir" ]]
	then
		echo $$ > $cg_dir/tasks
	fi
fi

R="$ECRYPTFS_CONTAINER"
env --default-signal "$R/lib64/ld-linux-x86-64.so.2" --library-path "$R/lib64" "$R/mount-in-container" --core "$R/mount-in-container" --dynamic-space-size 60 --noinform "$@"
