# File encryption via `ecryptfs` in RedHat OpenShift 4.12 and above

## What?

This project provides necessary configurations, templates, and instructions to make *filesystem-based encryption via `ecryptfs` available for your OpenShift containers*.


## Why?

To provide better data protection (though confidentiality only -- not availability nor integrity!), transparent encryption that the application doesn't even notice is a simple enhancement.


### Why `ecryptfs` and not ...?

There are a few competing products - Thales, `encFS`, `FSCrypt`, LUKS via `cryptsetup`, and so on.

We chose `ecryptfs` because:

- It encrypts individual files. This allows for easy incremental backups, `rsync`, and so on.
  Block device encryption (LUKS, or Thales) hides the filesystem structure in a big blob of many GB to TB - which makes backup/restore/etc. much harder.

- The encryption metadata is stored in the first 8 kiB of the actual (non-virtual) file. Here the big advantage is that the *encrypted* file (on the lower-level storage) can be transferred via `rsync`, `tar`, `ftp`, dragging in a file manager, HTTP, zip, etc.
  In contrast, `encFS` and `FSCrypt` use POSIX xattr for the per-file encryption key -- which is all too easily lost when moving stuff around.


## How? (Architecture)

This project uses the RedHat OpenShift /Kernel Module Manager/ operator to determine required `ecryptfs` module versions (which depend on the kernel versions and architectures in use), which uses the RedHat /Device Toolkit/ to compile and pack the kernel module into an image; KMM also runs the images on the nodes matching the kernel versions.

So our work here consists in the KMM configuration, the userspace part that gets run via an OCI hook [(1)](https://github.com/opencontainers/runtime-spec/blob/v1.0.2/config.md#posix-platform-hooks) [(2)](https://man.archlinux.org/man/oci-hooks.5.en), tests and documentation.

After going through the installation instructions below, you should be able to provide transparent encryption to your containers by a simple annotation (that triggers the OCI hook), a config map (that specifies which lower level storage directories should get decrypted to which destination paths), and one or more secrets (used as encryption key).

The instructions below also include a test container that should verify that everything works as expected.


## License and feedback

This work is provided under the GPL3; any feedback is welcome.

We won't be able to give installation support or any other guarantees, though.


# NOT SUPPORTED BY REDHAT

It's worth noting that RedHat doesn't support `ecryptfs` -- KMM and loading kernel modules is allowed, just the `ecryptfs` kernel module isn't available in CoreOS and this userspace solution isn't supported.

If you'd like to have them provide support, lobby them - like we did. You can reference the [(RFE 21636)](https://issues.redhat.com/browse/RHEL-21636).

# Installation

Please see [Installation](INSTALLATION.md).

# Usage

A general overview:

[overview](./doc/Interdependencies-eCryptFS-PaaS.svg)

1. The OCI hook gets triggered for a container via existence of an annotation
2. The userspace binary reads the config map(s) and referenced secret(s) and mounts `ecryptfs`
3. OCI runs the container application
4. When the container application stops, the mounts are cleaned up by the Linux kernel automatically

## *Attention - data loss when key is unavailable!*

The data becomes unavailable when the secret key or the salt are lost. So please think hard about your risk assessment -- doing key escrow, eg. paper that's stored in a safe, might be a good idea!


## Container configuration to _use_ `ecryptfs`

Please see the test container deployed via `openshift-configuration/test-deployment.yaml.template`.

- You need an annotation in the metadata; we're using a `nodeSelector` as well.

	```
	metadata:
      annotations:
        brz.gv.at/ecryptfs: "y"
    spec:
      nodeSelector:
        brz.gv.at/ecryptfs: "node"
	```

- The `volumes` needs to contain at least one config map (mounted somewhere below `/etc/brz.ecryptfs/`) and one secret (containing `key` and an optional `salt`)

- The config map tells the userspace binary which application directory (`app`) should get encrypted to which PV path (`storage`), and which key to use (`secret` points to the directory where the secret is mounted).

	```
	  config-map: |
		[{"app":"/mnt/application-data/",
		  "storage":"/mnt/pv/",
		  "secret":"/mnt/secret/"}]
	```

You can use the same `secret` path (and therefore key) for multiple mount points; and you can stack mount points within each other, the required mount order is determined automatically.

The `storage` directory will typically be a mount path of a PVC, but it doesn't have to be; it can also be an `emptyDir`, or specify a _subdirectory_ of a PVC, which is useful if the container will operate on just a subset of the data.

Also, `app` and `storage` can be the same path -- then the decrypted view is laid "on top" of the encrypted data. This way the application can use the same path, and the encrypted files are hidden - this is nice as it avoid accidentaly dropping clear-text data there.


## Signs of Misconfiguration

If the config map isn't valid JSON, contains invalid data, the secret key isn't found, etc., the userspace binary should complain in a way that the OpenShift dashboard shows in `Events`.

The messages should provide a clue; and in the worst case you can look at the sources as well ;)


## Check for "correct" encryption key (ie. the same as previously)

The current userspace binary includes a check that the "correct" secret (ie. the same as on the first run) is used on the PVC, so if you have the wrong secret configured you typically should get a nice error message before the container even starts (and corrupts existing data).

As `ecryptfs` encrypts filenames as well (with this userspace by default), two files are being used - one flag-file on the lower level storage and one above `ecryptfs`. If the first exists, then the other has to exist as well and contain some verifiable data -- else the container start is aborted to avoid using the wrong secret key on the mount point.

If you need to turn that sanity check off to manually investigate, add the flag `no-check-cipher` in the `brz.gv.at/ecryptfs` annotation of the container.


## Debug container

If there are any problems and you start a `Debug Container` (see the link in the `Logs` tab), this container doesn't have the needed annotation and therefore doesn't mount the `ecryptfs` directories.

We believe this to be a feature - it allows to debug the situation without the application without giving access to the decrypted data!

If that doesn't fit your needs, you might want to (temporarily) patch the `command` of the container to a shell.


# Performance

`ecryptfs` en- and decrypts data in the calling thread, so IO performance does suffer.

Your actual performance will depend on many details; for reference, here are our `fio` measurements -- done with an NFS storage, mounted as a normal PV. The red lines are with `ecryptfs`, the blue ones are the direct NFS performance as measured on the same host and in the same container.

The horizontal panels show thread numbers (1, 4, 16 Threads), the vertical use IO-depth (1 or 4, per thread).

## Bandwidth

[Bandwidth graph](./doc/bw.svg)

Even in the slowest variant (1 thread, io-depth 1, 4KB blocks) we already achieve a median value of 240MiB/sec; with 16 threads, io-depth 1, 1MiB block size we got 2.8GiB/sec.

## Latency

[Latency graph](./doc/lat.svg)

This shows an increase in latency that is proportional to the amount of data to be encrypted -- naturally.

## `fio` command

Here's an example command - the individual arguments varied from test to test. Argument combinations were iterated first, then the whole test run was repeated, so that 5 results for each input combination were collected.

```
# fio --name ecryptfs_nfs --filename /ecryptfs/nfs/pm.fio.testfile.j-1 --ioengine libaio --rw randwrite --time_based --group_reporting --direct 0 --size 400M --bs 1024k --runtime 10s --numjobs 1 --iodepth 1 --append-terse
```

## Container start

The additional OCI hook and the `ecryptfs` mounts take a bit of time, of course.

The userspace binary logs its runtime; it's typically about 0.2 seconds, so the total container startup overhead should be fairly small (below 1 second).

# Troubleshooting

Please see [Troubleshooting](TROUBLESHOOTING.md).
