# Installation

Some of the configuration files here can be used as-is; others, generally with `.template` in their name, need changes. Most of them are marked with a series of no less than three `X`, so searching for `XXX` in your favourite text editor should be a good first approximation where to fix stuff.


## Install the KMM operator

Login to your openshift console and install the RedHat /Kernel Module Manager/ operator (2.0.0 or later).

You can simply use the default `openshift-kmm` namespace.

Documentation for KMM is available here: https://docs.openshift.com/container-platform/4.12/hardware_enablement/kmm-kernel-module-management.html


## Setup a local mirror for RedHat Kernel Source RPMs

`ecryptfs` is compiled directly out of the RedHat Kernel Sources. This provides the best chance to get a working kernel module -- RedHat would either have to (more or less on purpose) break it or do some other bad magic, like using the C preprocessor to hide finding incompatibilities via ksyms (the (in)famous `#ifndef __GENKSYMS__` hack).

We're currently locally mirroring a few repositories; for OpenShift 4.14 on RHEL 9 for `amd64` CPUs, `rhocp-4.14-for-rhel-9-x86_64-source-rpms` is the right one -- it includes eg. `Packages/k/kernel-5.14.0-284.54.1.el9_2.src.rpm`.

Direct compilation of the kernel sources (with the included symbol versions or the `Symvers.gz` from the binary package) didn't work out, the kernel module couldn't be compiled resp. loaded into the running kernel, so we also use `dkms-3.0.12-1.el9.noarch.rpm` (from EPEL).

(We hope that RedHat will improve the situation.)


## Install the `ecryptfs` build instructions

Install the `openshift-configuration/ecryptfs-build-configmap.json´ file.


## Build the `ecryptfs` kernel module

Copy the module configuration file `openshift-configuration/ecryptfs-module.yaml.template` to `.yaml` (ie. without the `.template`), and fix the links to point to your local mirrors.
Install the changed file.

Please note that the file available here is only tested for OpenShift 4.14 on RHEL 9 (4.12 on RHEL8 is in the `git` history.)

Also, for architectures other than `x86_64` (like `arm64`) a few changes might be necessary.


## Get a current userspace rpm

Please navigate to the [CentOS `ecryptfs-utils` package page](https://cbs.centos.org/koji/packageinfo?packageID=8888) and fetch a package that matches the userspace base image (for an `ubi8` image on `amd64`, choose a matching `ecryptfs-utils-….el8.x86_64.rpm` - eg. get [ecryptfs-utils for el8, x86_64](https://cbs.centos.org/kojifiles/packages/ecryptfs-utils/111/15.3.el8/x86_64/ecryptfs-utils-111-15.3.el8.x86_64.rpm)), and store it in the subdirectory `userspace-daemonset/rpm/`.

This might need updating from time to time, though neither the kernel implementation nor the userspace should change much in the foreseeable future.

## Build the userspace binary

As the `ecryptfs` userspace needs to mount directories into a (potentially) untrusted container (who knows what `glibc` is installed there??), the userspace must be completely self-sufficient - after loading all necessary libraries it `chroot`s into the container namespace and must only read the bare minimum of data from there.
\\
Also, it has to do quite a few low-level library calls and even a `syscall` -- while I tried to use scripting languages (Python and Perl), it proved to be easiest to use SBCL and go with a single binary. (I don't know enough Go.)

The sources are available in `userspace-daemonset/src` (which need `alexandria` and `yason` as dependencies), but a binary is also available.

Don't be afraid of the binary size -- 30MB RAM are plenty for simple execution, and by re-using the `cgroup` of the daemonset it works in containers with 10MB RAM limits as well.


## Build the userspace image

Setup a build stream and a build config (see a potentially working one in `openshift-configuration/ecryptfs-userspace-buildconfig.yaml.template`), or use whatever process you prefer to create an image from `userspace-daemonset/Dockerfile`.


## Deploy the userspace daemonset

Apply `openshift-configuration/ecryptfs-userspace-daemonset.yaml`.

The daemonset exports the number of active `ecryptfs` mounts on port 9000, prometheus-compatible.

## Simple test to see whether it works

Create a small PVC (eg. on NFS), and put its name in a copy of `openshift-configuration/test-deployment.yaml.template` (or use an `emptyDir`) -- there are a few other points there to fix as well.

Look at the POD's console output to verify that `ecryptfs` works as expected.


