# Troubleshooting

## Wrong secret configured

If the wrong secret is configured, the `ecryptfs` kernel module might complain like this:

```
[1545921.039943] Could not find key with description: [some-key-id-here]
[1545921.047060] process_request_key_err: No key
[1545921.047061] ecryptfs_parse_tag_70_packet: Error attempting to find auth tok for fnek sig [some-key-id-here]; rc = [-2]
```

The current userspace binary includes a check that the same secret is used again, so typically you should get a nice error message before the container even starts.

## Running out of space on the encrypted storage

Running out of space on the underlying PVC means that `ecryptfs` can't write data any more; you'll get messages like this in `dmesg`:

```
[684235.206836] ecryptfs_write_begin: Error on attempt to truncate to (higher) offset [655360]; rc = [-28]
[684235.217714] ecryptfs_encrypt_page: Error attempting to write lower page; rc = [-28]
```

## Locking on NFS

`fcntl` and `flock` are currently not passed through to the lower-level filesystem; so while multiple containers resp. Pods can work on the same PV (eg. an RWX NFS volume), synchronization needs to happen either via locks in the lower-level mountpoint or via other means.

See the bug report at [[https://bugs.launchpad.net/ecryptfs/+bug/2012301]].

## Memory cgroup

To make this mechanism work for containers with a RAM limit as small as 10MB (I got it working with 6MB, basically what `bash` needs), the installed OCI hook first runs a shell script which switches the `memory` cgroup over to the userspace daemonset; the single binary executable requires about 30MB RAM to load successfully.


## Encryption secrets get hidden by default

The secrets used to mount `ecryptfs` directories get hidden by mounting an empty, read-only, zero size `tmpfs` for security reasons.

If the container annotation contains `keep-secrets`, this is skipped.


## Debugging

You can debug the mount operations as follows:

- Passing `trace` resp. `debug` as first argument (best done by modifying OCI hook definition)
- Setting the environment variable `ECRYPTFS_TRACE` (add it in the OCI hook definition)
- Having keywords `trace` and/or `debug` in the `at.gv.brz/ecryptfs` annotation, either alone and/or comma separated


For `debug`, a few intermittent lines will be logged, most probably to the `systemd` logs to be queried via`journalctl`.

For `trace`, also the function call hierarchy with arguments and return values will be dumped - this can get a little unwieldy to look at.

